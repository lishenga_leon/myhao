# Base Image
FROM python:3.6

# create and set working directory
RUN mkdir /app
WORKDIR /app

# Add current directory code to working directory
ADD . /app/

# COPY ./requirements.txt /app/requirements.txt

# set default environment variables
ENV PYTHONUNBUFFERED 1
ENV LANG C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive 

# set project environment variables
# grab these via Python's os.environ
# these are 100% optional here
ENV PORT=3761

# Install system dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
        tzdata \
        python3-setuptools \
        python3-pip \
        python3-dev \
        python3-venv \
        git \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*


# install environment dependencies
RUN pip3 install --upgrade pip 

# Install project dependencies
RUN pip3 install -r requirements.txt

EXPOSE 3761
CMD gunicorn myhao.wsgi:application --bind 0.0.0.0:$PORT




# pull official base image
# FROM python:3.7.4-alpine

# # set work directory
# WORKDIR /usr/src/app

# # set environment variables
# ENV PYTHONDONTWRITEBYTECODE 1
# ENV PYTHONUNBUFFERED 1

# RUN apk update \
#     && apk add --virtual build-deps gcc python3-dev musl-dev \
#     && apk add postgresql-dev \
#     && pip install psycopg2 \
#     && apk del build-deps

# # install dependencies
# RUN pip install --upgrade pip gunicorn 

# COPY ./requirements.txt /usr/src/myhao/requirements.txt

# RUN pip install -r requirements.txt

# #Gie Permission and make executable
# RUN chmod 755 /usr/src/myhao/entrypoint.sh

# COPY . /usr/src/app


# RUN ["chmod", "+x", "/usr/src/myhao/entrypoint.sh"]

# # run entrypoint.sh
# ENTRYPOINT ["/usr/src/myhao/entrypoint.sh"]
