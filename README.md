Purpose:
To test your understanding and how comfortable you are with ​web development and your creativity.
In addition we seek to establish the following:
- Test Driven development approach to your development.
- Use of Logging
- Use of CI/CD tool of your choice
- Use of version controlling (Git/Github/Bitbucket etc)
- Any other best practices inline with development and devops will be an
added advantage.
Tools:
Preferably PHP/LARAVEL/MYSQL , however you can also use another framework you are comfortable with (Django/NodeJS/Flask etc) / any db of your choice for this assignment. However the real work project is in PHP/Laravel/MYSQL but who knows we might shift it to your choice of stack.
Task:
1) Project/Portal name - myhao (Come up with your own logo and colour themes)
2) myhao wants to create a web portal that allows the ​public​ to enlist for
property ownership. Part of your portal should allow for public registration.
The portal verifies a user's ID via IPRS and the interested user must also
make a payment of 500KES via mpesa upon which they continually save
until they have arrived at at least 10% of the value of the property they wish
to purchase. A user can save for several years or months and they should
have a record via the portal of their savings. A user's registration is
confirmed via SMS and email with a verification code enabling them to
formally enroll.
(For the purpose of this assignment the IPRS verification and the MPESA integration/SMS integration is not necessarily due to the short nature of the assignment)​.
3) The portal should also accommodate ​property developers​ to enlist with pictures some of the properties they have on offer. So each developer will create their profile and upload property offerings. Essentially a property developer will have one or many developments. Each development will have its location details(e.g Kindaruma Road, Off Ngong Road, Nairobi) and the offerings e.g 1 br, 2br, 3br, Studio Apartments etc with the sizes of each features and prices. Property Developers regularly update on the progress of each of their projects.
  
4) Theportalshouldaccommodatebanks/sacco’storegisteraswell.These are the financiers of the clients seeking property ownership. Clients can choose a financier of their choice, in which case once they get to 10% of a property of their liking , this request will be delivered to the bank/sacco who in-turn begin processing a mortgage for this user and at each stage they update the site with the status of the application.
5) The portal should have a basic CRM to showcase at what stage an application has reached and should offer suggestions based on the users preferred home location and how far they are to reaching the 10% of any property listing within their locality. The CRM also has any correspondences made with the client. Feel free to add on extra features which you think maybe necessarily.