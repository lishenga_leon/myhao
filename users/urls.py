from django.contrib import admin
from django.urls import path, include
from . import views


urlpatterns = [
    path('dashboard', views.index, name="dashboard"),
    path('', views.index, name='login'),
    path('login', views.do_login, name='login'),
    path('logout', views.logout, name='logout'),
    path('register', views.register, name='register'),
    path('makepayment', views.make_payment, name='makePayment'),
    path('userpayments', views.user_payments, name='userPayments'),
    path('usersavings', views.user_savings, name='userSavings'),
    
    path('property/', include('properties.urls')),
]