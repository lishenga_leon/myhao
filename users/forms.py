from django.forms import ModelForm
from django import forms

from .models import Users

class UserPaymentsFilterForm(forms.Form):
    id__icontains = forms.IntegerField(label='', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control col-md-3 data-table-filter-input',
        'placeholder': 'ID ..'

    }))

    properties_id__icontains = forms.CharField(label='', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control col-md-3 data-table-filter-input',
        'placeholder': 'Property ID ..'

    }))

class CreateUserForm(ModelForm):
    class Meta:
        model = Users
        fields = ['first_name', 'last_name', 'email', 'user_role', 'password']