from django.shortcuts import redirect, render
from django.contrib.auth.hashers import make_password
from passlib.hash import django_pbkdf2_sha256 as password_handler
from django.contrib.auth import login, update_session_auth_hash, logout as django_logout
from vendor.django_datatables import column
from vendor.django_datatables.datatable import *
from .models import Users, Payments, Savings
from financier.models import Financier
from .authenticate import MyHaoAuthBackend
from .forms import UserPaymentsFilterForm
from properties.models import Property, PublicUserProperty
from django.contrib import messages
from django.db.models import F, Q, Sum, Value
import pytz, json
import datetime
from django.contrib.auth.decorators import login_required



class UserPaymentsListDatatable(Datatable):
    property_name = column.StringColumn()
    financier_name = column.StringColumn()
    amount = column.StringColumn()
    status = column.StringColumn()
    created_at = column.StringColumn()
    updated_at = column.StringColumn()

    def __init__(self, datapy=None):
        self.datapy = datapy

    def get_initial_queryset(self, request):
        self.datapy = json.loads(request.GET.get('extras'))
        return Payments.objects.filter(user_id=self.datapy['id'])

    def render_property_name(self, row):
        return Property.objects.get(id=row['properties_id']).name

    def render_financier_name(self, row):
        return Financier.objects.get(id=row['financier_id']).name

    def render_amount(self, row):
        return f'KSH '+ str(row['amount'])

    def render_status(self, row):
        if row['status'] == 1:
            return f'Success'
        else:
            return f'Failed'

    def render_created_at(self, row):
        return row['created_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_updated_at(self, row):
        return row['updated_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    class Meta:
        model = Payments
        filter_form = UserPaymentsFilterForm
        extra_fields = ('id', 'created_at', 'updated_at', 'status', 'amount', 'properties_id', 'financier_id')


class UserSavingsListDatatable(Datatable):
    property_name = column.StringColumn()
    financier_name = column.StringColumn()
    amount = column.StringColumn()
    percentage = column.StringColumn()
    status = column.StringColumn()
    created_at = column.StringColumn()
    updated_at = column.StringColumn()

    def __init__(self, datapy=None):
        self.datapy = datapy

    def get_initial_queryset(self, request):
        self.datapy = json.loads(request.GET.get('extras'))
        return Savings.objects.filter(user_id=self.datapy['id'])

    def render_property_name(self, row):
        return Property.objects.get(id=row['properties_id']).name

    def render_financier_name(self, row):
        return Financier.objects.get(id=row['financier_id']).name

    def render_amount(self, row):
        return f'KSH '+ str(row['amount'])

    def render_percentage(self, row):
        return str(row['percentage']) + f'% Paid'

    def render_status(self, row):
        if row['status'] == 1:
            return f'Success'
        else:
            return f'Failed'

    def render_created_at(self, row):
        return row['created_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_updated_at(self, row):
        return row['updated_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    class Meta:
        model = Savings
        filter_form = UserPaymentsFilterForm
        extra_fields = ('id', 'created_at', 'updated_at', 'status', 'amount', 'properties_id', 'financier_id', 'percentage')




# Create your views here.
def index(request):
    if request.user.is_authenticated:
        return render(request, 'users/dashboard.html', {})
    return redirect('login')

def do_login(request):
    if request.user.is_authenticated:
        return redirect('/')
    else:
        template = 'authentication/login.html'
        if request.method == 'POST':
            try:
                email=request.POST['email']
                password=request.POST['password']
                user=Users.objects.get(email=email)

                users = MyHaoAuthBackend.doAuthenticate(request=request,
                                                    username=email, password=password)

                if users is not None:
                    login(request, user)
                    messages.add_message(request, messages.SUCCESS,
                                    'Success')
                    return redirect('/dashboard')

                else:
                    messages.add_message(request, messages.ERROR,
                                    'Incorrect Credentials')    
            except BaseException as e :
                messages.add_message(request, messages.ERROR,
                                    e)
        return render(request, template, {})
    

def register(request):
    if request.method == 'POST':
        if request.POST['user_role'] == 'BANK':
            create = Financier(name=request.POST['name'], created_at=datetime.datetime.now(tz=pytz.UTC), updated_at=datetime.datetime.now(tz=pytz.UTC)).save()
            bank = Financier.objects.get(name=request.POST['name'])
            user = Users(first_name=request.POST['name'], username=request.POST['email'], last_name=request.POST['name'], financier = bank.id, email=request.POST['email'], password=make_password(request.POST['password']), user_role='BANK', created_at=datetime.datetime.now(tz=pytz.UTC), updated_at=datetime.datetime.now(tz=pytz.UTC))
            user.save()
            messages.add_message(request, messages.SUCCESS,
                                'User Registered')
            return redirect('/login')
        else:
            user = Users(first_name=request.POST['first_name'], username=request.POST['email'], last_name=request.POST['last_name'], email=request.POST['email'], password=make_password(request.POST['password']), user_role=request.POST['user_role'], created_at=datetime.datetime.now(tz=pytz.UTC), updated_at=datetime.datetime.now(tz=pytz.UTC))
            user.save()
            messages.add_message(request, messages.SUCCESS,
                                'User Registered')
            return redirect('/login')

    return render(request, 'authentication/register.html', { })


@login_required
def logout(request):
    django_logout(request)
    return redirect('/login')


@login_required
def make_payment(request):
    create = Payments(user_id=request.user.id, properties_id=request.POST['propertyId'], financier_id=request.POST['financier'], amount=request.POST['amount'], created_at=datetime.datetime.now(tz=pytz.UTC), updated_at=datetime.datetime.now(tz=pytz.UTC))
    create.save() 
    prop = Property.objects.get(id=request.POST['propertyId'])
    if Savings.objects.filter(user_id=request.user.id).filter(properties_id=request.POST['propertyId']).exists() and request.POST['amount'] is not None:
        agg = Payments.objects.filter(user_id=request.user.id).get(properties_id=request.POST['propertyId']).aggregate(total=(Sum('amount')))
        percent = ((int(request.POST['amount'])+int(agg['total']))*100)/int(prop.price)
        save = Savings.objects.filter(user_id=request.user.id).get(properties_id=request.POST['propertyId'])
        save.percentage = percent
        save.amount = (request.POST['amount']+agg['total'])
        save.updated_at=datetime.datetime.now(tz=pytz.UTC)
        save.save()
        return redirect('property/user')
    else:
        percent = (int(request.POST['amount'])*100)/int(prop.price)
        save = Savings(
            user_id=request.user.id,
            properties_id=request.POST['propertyId'], 
            financier_id=request.POST['financier'],
            amount = request.POST['amount'],
            percentage = percent,
            status = 1,
            created_at=datetime.datetime.now(tz=pytz.UTC), 
            updated_at=datetime.datetime.now(tz=pytz.UTC)
        )
        save.save()
        PublicUserProperty(user_id=request.user.id, properties_id=request.POST['propertyId'], status=1,
            created_at=datetime.datetime.now(tz=pytz.UTC), 
            updated_at=datetime.datetime.now(tz=pytz.UTC)).save()
        return redirect('property/user')


@login_required
def user_payments(request):
    datatable = UserPaymentsListDatatable()
    datatable.extras = json.dumps({"id": request.user.id})
    return render(request, 'users/userpayments.html', { 
        'datatable': datatable,
    })

@login_required
def user_savings(request):
    datatable = UserSavingsListDatatable()
    datatable.extras = json.dumps({"id": request.user.id})
    return render(request, 'users/userpayments.html', { 
        'datatable': datatable,
    })
