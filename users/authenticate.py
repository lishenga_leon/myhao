from django.contrib.auth import backends, get_user_model
from passlib.hash import django_pbkdf2_sha256 as password_handler
from django.db.models import Q

UserModel = get_user_model()


class MyHaoAuthBackend(backends.ModelBackend):
    '''
        Customized authentication backend
    '''

    def authenticate(self, request, username=None, password=None, **kwargs):
        try:
            user = UserModel.objects.get(
                Q(username__iexact=username) | Q(email__iexact=username))

        except UserModel.DoesNotExist:
            return None
        else:
            if password_handler.verify(password, user.password):
                return user
        return None

    @staticmethod
    def doAuthenticate(request, username=None, password=None, **kwargs):
        try:
            user = UserModel.objects.get(
                Q(username__iexact=username) | Q(email__iexact=username))

        except UserModel.DoesNotExist:
            return None
        else:
            if password_handler.verify(password, user.password):
                return user
        return None
