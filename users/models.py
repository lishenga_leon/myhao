from django.db import models
from financier.models import Financier
from properties.models import Property
from django.contrib.auth.models import AbstractUser
import datetime


class Users(AbstractUser):

    PUBLIC = 'PUBLIC'
    PROPERTY_OWNER = 'PROPERTY_OWNER'
    ROLE = [
        (PUBLIC, ('Public')),
        (PROPERTY_OWNER, ('Property Owner')),
    ]
    last_login = None
    
    first_name = models.CharField(max_length=255, default=None)
    username = models.CharField(max_length=250, unique=True)
    last_name = models.CharField(max_length=255, default=None)
    email = models.CharField(max_length=255, default=None, unique= True)
    password = models.CharField(max_length=255, default=None)
    financier = models.IntegerField(default=0)
    status = models.IntegerField(default=0)
    user_role = models.CharField(max_length=255, choices=ROLE)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)



class Payments(models.Model):
    
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    properties = models.ForeignKey(Property, on_delete=models.CASCADE)
    financier = models.ForeignKey(Financier, on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)
    status = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)

class Savings(models.Model):
    
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)
    properties = models.ForeignKey(Property, on_delete=models.CASCADE)
    financier = models.ForeignKey(Financier, on_delete=models.CASCADE)
    percentage = models.IntegerField(default=0)
    status = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)
