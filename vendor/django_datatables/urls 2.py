from __future__ import unicode_literals
from django.urls import path

from .views import datatable_manager

app_name = 'django_datatables'

urlpatterns = [
    path('data/', datatable_manager, name="datatable_manager")
]
