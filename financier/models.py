from django.db import models
import datetime
# Create your models here.

class Financier(models.Model):
    
    name = models.CharField(max_length=255, default=None, unique=True)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)


class UserFinanciers(models.Model):
    
    financier = models.ForeignKey(Financier, on_delete=models.CASCADE)
    user = models.ForeignKey('users.Users', on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)