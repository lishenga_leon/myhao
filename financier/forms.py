from django import forms

class UserFinanciersFilterForm(forms.Form):
    id__icontains = forms.IntegerField(label='', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control col-md-3 data-table-filter-input',
        'placeholder': 'ID ..'

    }))

    name__icontains = forms.CharField(label='', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control col-md-3 data-table-filter-input',
        'placeholder': 'Name ..'

    }))

class AllFinanciersFilterForm(forms.Form):
    id__icontains = forms.IntegerField(label='', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control col-md-3 data-table-filter-input',
        'placeholder': 'ID ..'

    }))

    name__icontains = forms.CharField(label='', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control col-md-3 data-table-filter-input',
        'placeholder': 'Name ..'

    }))


class BankFinanciersFilterForm(forms.Form):
    id__icontains = forms.IntegerField(label='', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control col-md-3 data-table-filter-input',
        'placeholder': 'ID ..'

    }))

    email__icontains = forms.CharField(label='', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control col-md-3 data-table-filter-input',
        'placeholder': 'Email ..'

    }))