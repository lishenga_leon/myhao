from django.shortcuts import redirect, render
from vendor.django_datatables import column
from vendor.django_datatables.datatable import *
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, update_session_auth_hash
from django.contrib.auth.hashers import make_password
from .models import Financier, UserFinanciers
from .forms import UserFinanciersFilterForm, AllFinanciersFilterForm, BankFinanciersFilterForm
from users.models import Users, Savings
from django.contrib import messages
from properties.models import PublicUserProperty
import pytz
import datetime
import json

# Create your views here
class UserFinanciersListDatatable(Datatable):
    name = column.StringColumn()
    created_at = column.StringColumn()
    updated_at = column.StringColumn()
    action = column.StringColumn()

    def __init__(self, datapy=None):
        self.datapy = datapy

    def get_initial_queryset(self, request):
        self.datapy = json.loads(request.GET.get('extras'))
        return UserFinanciers.objects.filter(user_id=self.datapy['id'])

    def render_name(self, row):
        return Financier.objects.get(id=row['financier_id']).name

    def render_created_at(self, row):
        return Financier.objects.get(id=row['financier_id']).created_at.strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_updated_at(self, row):
        return Financier.objects.get(id=row['financier_id']).updated_at.strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_action(self, row):
        return f"<div class='badge badge-danger' data-toggle='modal' data-target='#removeFinancier{row['id']}' style='margin-right: 10px'>Remove<i class='far fa-trash-alt'></i></div>"


    class Meta:
        model = UserFinanciers
        filter_form = UserFinanciersFilterForm
        extra_fields = ('id', 'created_at', 'updated_at', 'user_id', 'financier_id')


class AllFinanciersListDatatable(Datatable):
    name = column.TextColumn()
    created_at = column.StringColumn()
    updated_at = column.StringColumn()
    action = column.StringColumn()


    def render_created_at(self, row):
        return row['created_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_updated_at(self, row):
        return row['updated_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_action(self, row):
        return f"<div class='badge badge-success' data-toggle='modal' data-target='#updateFinancier{row['id']}' style='margin-right: 10px'>Edit <i class='fas fa-edit'></i></div><a href='financier/{row['id']}/delete'><div class='badge badge-danger' style='margin-right: 10px'>Delete<i class='far fa-trash-alt'></i></div></a>"

    class Meta:
        model = Financier
        filter_form = AllFinanciersFilterForm
        extra_fields = ('id', 'created_at', 'updated_at')



class AvailableFinanciersUserPropertyListDatatable(Datatable):
    name = column.TextColumn()
    created_at = column.StringColumn()
    updated_at = column.StringColumn()
    action = column.StringColumn()

    def __init__(self, datapy=None):
        self.datapy = datapy

    def get_initial_queryset(self, request):
        self.datapy = json.loads(request.GET.get('extras'))
        query = UserFinanciers.objects.filter(user_id=self.datapy['id']).values_list('financier_id', flat=True)
        return Financier.objects.exclude(id__in=query)

    def render_created_at(self, row):
        return row['created_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_updated_at(self, row):
        return row['updated_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_action(self, row):
        return f"<div class='badge badge-info' data-toggle='modal' data-target='#makeFinancier{row['id']}' style='margin-right: 10px'>Make Financier<i class='fas fa-edit'></i></div>"


    class Meta:
        model = Financier
        filter_form = UserFinanciersFilterForm
        extra_fields = ('id', 'created_at', 'updated_at')



class SpecificFinanciersUserPropertyListDatatable(Datatable):
    name = column.TextColumn()
    created_at = column.StringColumn()
    updated_at = column.StringColumn()
    action = column.StringColumn()


    def render_created_at(self, row):
        return row['created_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_updated_at(self, row):
        return row['updated_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_action(self, row):
        return f"<div class='badge badge-success' data-toggle='modal' data-target='#updateFinancier{row['id']}' style='margin-right: 10px'>Edit <i class='fas fa-edit'></i></div><a href='financier/{row['id']}/delete'><div class='badge badge-danger' style='margin-right: 10px'>Delete<i class='far fa-trash-alt'></i></div></a>"

    class Meta:
        model = Financier
        filter_form = AllFinanciersFilterForm
        extra_fields = ('id', 'created_at', 'updated_at')


class BankUserPropertyListDatatable(Datatable):
    name = column.StringColumn()
    email = column.StringColumn()
    savings = column.StringColumn()
    percentage = column.StringColumn()
    morgage = column.StringColumn()
    created_at = column.StringColumn()
    updated_at = column.StringColumn()
    action = column.StringColumn()

    def __init__(self, datapy=None):
        self.datapy = datapy

    def get_initial_queryset(self, request):
        self.datapy = json.loads(request.GET.get('extras'))
        return UserFinanciers.objects.filter(financier_id=self.datapy['id'])

    def render_name(self, row):
        user = Users.objects.get(id=row['user_id'])
        return user.first_name + ' '+ user.last_name

    def render_email(self, row):
        user = Users.objects.get(id=row['user_id'])
        return user.email

    def render_savings(self, row):
        query = PublicUserProperty.objects.filter(user_id=row['user_id']).values_list('properties_id', flat=True)
        fina = UserFinanciers.objects.filter(user_id=row['user_id']).values_list('financier_id', flat=True)
        if Savings.objects.filter(properties_id__in=query).filter(financier_id__in=fina).filter(user_id=row['user_id']).exists():
            return Savings.objects.filter(properties_id__in=query).filter(financier_id__in=fina).get(user_id=row['user_id']).amount
        else:
            return f'No Savings'

    def render_percentage(self, row):
        query = PublicUserProperty.objects.filter(user_id=row['user_id']).values_list('properties_id', flat=True)
        fina = UserFinanciers.objects.filter(user_id=row['user_id']).values_list('financier_id', flat=True)
        if Savings.objects.filter(properties_id__in=query).filter(financier_id__in=fina).filter(user_id=row['user_id']).exists():
            return Savings.objects.filter(properties_id__in=query).filter(financier_id__in=fina).get(user_id=row['user_id']).percentage
        else:
            return 0

    def render_morgage(self, row):
        query = PublicUserProperty.objects.filter(user_id=row['user_id']).values_list('properties_id', flat=True)
        fina = UserFinanciers.objects.filter(user_id=row['user_id']).values_list('financier_id', flat=True)
        if Savings.objects.filter(properties_id__in=query).filter(financier_id__in=fina).filter(user_id=row['user_id']).exists():
            save = Savings.objects.filter(properties_id__in=query).filter(financier_id__in=fina).get(user_id=row['user_id']).percentage
            if save >= 10:
                return f"<a href='financier/{row['user_id']}/morgage'><div class='badge badge-success'><i class='mdi mdi-eye'></i>Give Morgage</div></a>"
            else:
                f'Not Available'
        else:
            return f'Not Available'

    def render_created_at(self, row):
        return row['created_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_updated_at(self, row):
        return row['updated_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_action(self, row):
        return f"<div class='badge badge-info' data-toggle='modal' data-target='#makeFinancier{row['id']}' style='margin-right: 10px'>Make Financier<i class='fas fa-edit'></i></div>"


    class Meta:
        model = UserFinanciers
        filter_form = BankFinanciersFilterForm
        extra_fields = ('id', 'created_at', 'updated_at', 'user_id')    



class Financiers():
    @login_required
    def index(request):
        if request.user.user_role == 'Public':
            datatable = UserFinanciersListDatatable()
            datatable.extras = json.dumps({"id": request.user.id})
            users_fin = UserFinanciers.objects.filter(user_id=request.user.id).values_list('financier_id', flat=True)
            fin = Financier.objects.filter(id__in = users_fin)
            return render(request, 'financier/userfinanciers.html', { 
                'datatable': datatable,
                'financiers': fin
            })
        elif request.user.user_role == 'BANK':
            datatable = SpecificFinanciersUserPropertyListDatatable()
            fina = Financier.objects.all()
            return render(request, 'financier/allfinanciers.html', { 
                'datatable': datatable,
                'financiers': fina
            })
        else:
            datatable = AllFinanciersListDatatable()
            fina = Financier.objects.all()
            return render(request, 'financier/allfinanciers.html', { 
                'datatable': datatable,
                'financiers': fina
            })

    @login_required
    def user_available_financiers(request):
        datatable = AvailableFinanciersUserPropertyListDatatable()
        datatable.extras = json.dumps({"id": request.user.id})
        query = UserFinanciers.objects.filter(user_id=request.user.id).values_list('financier_id', flat=True)
        return render(request, 'financier/availablefinanciersusers.html', { 
            'datatable': datatable,
            'financiers': Financier.objects.exclude(id__in=query)
        })


    @login_required
    def create(request):
        if request.method == 'POST':
            Financier(name=request.POST['name'], created_at=datetime.datetime.now(tz=pytz.UTC), updated_at=datetime.datetime.now(tz=pytz.UTC)).save()
            bank = Financier.objects.get(name=request.POST['name'])
            Users(first_name=request.POST['name'], username=request.POST['email'], last_name=request.POST['name'], financier = bank.id, email=request.POST['email'], password=make_password(request.POST['password']), user_role='BANK', created_at=datetime.datetime.now(tz=pytz.UTC), updated_at=datetime.datetime.now(tz=pytz.UTC)).save()
            messages.add_message(request, messages.SUCCESS,
                                'Financier Created')
            return redirect('userFinancier')

        return redirect('userFinancier')


    @login_required
    def update(request):
        if request.method == 'POST':
            update = Financier.objects.get(id=request.POST['financeId'])
            user = Users.objects.get(financier_id=request.POST['financeId'])
            if request.POST['name'] is not None:
                update.name = request.POST['name']
                user.first_name = request.POST['name']
                user.last_name = request.POST['name']
            if request.POST['email'] is not None:
                user.email = request.POST['email']
            if request.POST['password'] is not None:
                user.password = make_password(request.POST['password'])
            update.updated_at=datetime.datetime.now(tz=pytz.UTC)
            user.updated_at=datetime.datetime.now(tz=pytz.UTC)
            update.save()
            user.save()
            messages.add_message(request, messages.SUCCESS,
                                'Financier Updated')
            return redirect('userFinancier')

        return redirect('userFinancier')


    @login_required
    def delete(request, _id):
        delete = Financier.objects.get(id=_id)
        delete.delete()
        messages.add_message(request, messages.SUCCESS,
                                'Financier Deleted')
        return redirect('userFinancier')


    @login_required
    def make_financier(request):
        if request.method == 'POST':
            UserFinanciers(user_id=request.user.id, financier_id=request.POST['financierId'], created_at=datetime.datetime.now(tz=pytz.UTC), updated_at=datetime.datetime.now(tz=pytz.UTC)).save()
            messages.add_message(request, messages.SUCCESS,
                                    'Added Financier')
            return redirect('userFinancier')
        return redirect('userFinancier')

    @login_required
    def remove_financier(request):
        if request.method == 'POST':
            UserFinanciers.objects.filter(id=request.POST['financierId']).filter(user_id=request.user.id).delete()
            messages.add_message(request, messages.SUCCESS,
                                    'Financier Removed')
            return redirect('userFinancier')
        return redirect('userFinancier')


    @login_required
    def bank_Users(request):
        datatable = BankUserPropertyListDatatable()
        fin = Financier.objects.get(name=request.user.first_name)
        datatable.extras = json.dumps({"id": fin.id})
        return render(request, 'financier/bankusers.html', { 
            'datatable': datatable,
        })


    @login_required
    def give_morgage(request, _id):
        morgage = PublicUserProperty.objects.get(user_id=_id)
        morgage.morgage = 'Available'
        morgage.updated_at=datetime.datetime.now(tz=pytz.UTC)
        morgage.save()
        return redirect('bankUsers')
