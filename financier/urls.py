from django.contrib import admin
from django.urls import path, include
from . import views


urlpatterns = [

    # property urls
    path('user', views.Financiers.index, name="userFinancier"),
    path('availablefinanciers', views.Financiers.user_available_financiers, name="availableFinanciers"),
    path('create', views.Financiers.create, name="createFinancier"),
    path('update/', views.Financiers.update, name="updateFinancier"),
    path('financier/<int:_id>/delete/', views.Financiers.delete, name="deleteFinancier"),
    path('makefinancier/', views.Financiers.make_financier, name="makeFinancier"),
    path('removefinancier/', views.Financiers.remove_financier, name="removeFinancier"),
    path('bankusers/', views.Financiers.bank_Users, name="bankUsers"),
    path('financier/<int:_id>/morgage/', views.Financiers.give_morgage, name="giveMorgage"),
]