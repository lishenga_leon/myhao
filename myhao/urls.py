"""myhao URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from users import views as users
from properties import views as properties


urlpatterns = [

    path('', include('users.urls')),

    # property urls
    path('property/property/<int:_id>/update/', properties.Property.update, name="updateProperty"),
    path('property/property/<int:_id>/delete/', properties.Property.delete, name="deleteProperty"),

    #property details
    path('property/propertydetail/<int:_id>', properties.PropertyDetails.index, name="propertyDetails"),
    path('property/propertydetail/update/', properties.PropertyDetails.update, name="updatePropertyDetail"),
    path('property/propertydetail/<int:_id>/delete/<int:property_id>', properties.PropertyDetails.delete, name="deletePropertyDetail"),

    path('__django_datatables__/', include('vendor.django_datatables.urls')),

    path('financier/', include('financier.urls')),
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
    # urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)