from django.contrib import admin
from django.urls import path, include
from . import views


urlpatterns = [

    # property urls
    path('user', views.Property.index, name="userProperty"),
    path('availableproperties', views.Property.user_available_properties, name="publicAvailableProperties"),
    path('create', views.Property.create, name="createProperty"),
    path('<int:_id>/update/', views.Property.update, name="updateProperty"),
    path('<int:_id>/delete/', views.Property.delete, name="deleteProperty"),
    path('<int:_id>/images/', views.Property.get_property_images, name="propertyImages"),
    path('uploadimages/', views.Property.upload_property_images, name="uploadPropertyPics"),

    # property details urls
    path('propertydetail/<int:_id>', views.PropertyDetails.index, name="propertyDetails"),
    path('propertydetail/create', views.PropertyDetails.create, name="createPropertyDetail"),
    path('propertydetail/update/', views.PropertyDetails.update, name="updatePropertyDetail"),
    path('propertydetail/<int:_id>/delete/<int:property_id>', views.PropertyDetails.delete, name="deletePropertyDetail"),
]