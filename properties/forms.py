from django import forms

class PropertyFilterForm(forms.Form):
    id__icontains = forms.IntegerField(label='', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control col-md-3 data-table-filter-input',
        'placeholder': 'ID ..'

    }))

    name__icontains = forms.CharField(label='', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control col-md-3 data-table-filter-input',
        'placeholder': 'Name ..'

    }))

class PropertyDetailsFilterForm(forms.Form):
    id__icontains = forms.IntegerField(label='', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control col-md-3 data-table-filter-input',
        'placeholder': 'ID ..'

    }))

    name__icontains = forms.CharField(label='', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control col-md-3 data-table-filter-input',
        'placeholder': 'Name ..'

    }))

class PublicUserPropertyFilterForm(forms.Form):
    id__icontains = forms.IntegerField(label='', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control col-md-3 data-table-filter-input',
        'placeholder': 'ID ..'

    }))

    name__icontains = forms.CharField(label='', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control col-md-3 data-table-filter-input',
        'placeholder': 'Name ..'

    }))