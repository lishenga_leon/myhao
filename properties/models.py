from django.db import models
from financier.models import Financier
import datetime
from users import models as user_model


class Property(models.Model):
    
    name = models.CharField(max_length=255, default=None)
    user = models.ForeignKey('users.Users', related_name='users_property', on_delete=models.CASCADE)
    city = models.CharField(max_length=255, default=None)
    price = models.CharField(max_length=255, default=0)
    country = models.CharField(max_length=255, default=None)
    street = models.CharField(max_length=255, default=None)
    status = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)

    @property
    def get_property(self):
        return Property.objects.get(id=self.id)


class PublicUserProperty(models.Model):
    
    user = models.ForeignKey('users.Users', on_delete=models.CASCADE)
    properties = models.ForeignKey(Property, on_delete=models.CASCADE)
    morgage = models.CharField(max_length=255, default='Unavailable')
    status = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)



class PropertyPictures(models.Model):
    
    name = models.FileField(upload_to='propertypics/')
    properties = models.ForeignKey(Property, on_delete=models.CASCADE)
    status = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)

class PropertyDetails(models.Model):
    
    name = models.CharField(max_length=255, default=None)
    units = models.CharField(max_length=255, default=None)
    price = models.CharField(max_length=255, default=0)
    size = models.CharField(max_length=255, default=None)
    properties = models.ForeignKey(Property, on_delete=models.CASCADE)
    status = models.CharField(max_length=255, default=None)
    progress = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)