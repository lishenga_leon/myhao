from django.shortcuts import redirect, render
from django.contrib.auth.hashers import make_password
from passlib.hash import django_pbkdf2_sha256 as password_handler
from vendor.django_datatables import column
from vendor.django_datatables.datatable import *
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, update_session_auth_hash
from .models import Property as PropertyModel, PropertyPictures as PropertyPicturesModel, PropertyDetails as PropertyDetailsModel, PublicUserProperty as PublicUserPropertyModel
from .forms import PropertyFilterForm, PropertyDetailsFilterForm, PublicUserPropertyFilterForm
from users.models import Users, Savings
from financier.models import Financier, UserFinanciers
from django.contrib import messages
import pytz
import datetime
import json
from django.http import JsonResponse

# Create your views here
class UserPropertyListDatatable(Datatable):
    name = column.TextColumn()
    price = column.StringColumn()
    street = column.TextColumn()
    city = column.TextColumn()
    country = column.TextColumn()
    status = column.StringColumn()
    images = column.StringColumn()
    created_at = column.StringColumn()
    updated_at = column.StringColumn()
    view = column.StringColumn()

    def __init__(self, datapy=None):
        self.datapy = datapy

    def get_initial_queryset(self, request):
        self.datapy = json.loads(request.GET.get('extras'))
        return PropertyModel.objects.filter(user_id=self.datapy['id'])

    def render_price(self, row):
        return f'KSH '+ str(row['price'])

    def render_status(self, row):
        if row['status'] == 1:
            return f'Active'
        else:
            return f'Inactive'

    def render_images(self, row):
        if PropertyPicturesModel.objects.filter(properties_id=row['id']).exists():
            return f"<div class='badge badge-success' data-toggle='modal' onClick='getImages({row['id']})' data-backdrop='static' data-keyboard='false' data-target='#propertyImages{row['id']}' style='margin-right: 10px'>View <i class='fas fa-edit'></i></div>"
        else:
            return f"<div class='badge badge-danger'>Upload Images</div>"

    def render_created_at(self, row):
        return row['created_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_updated_at(self, row):
        return row['updated_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_view(self, row):
        return f"<div class='badge badge-success' data-toggle='modal' data-target='#updateProperty{row['id']}' style='margin-right: 10px'>Edit <i class='fas fa-edit'></i></div><a href='property/{row['id']}/delete'><div class='badge badge-danger' style='margin-right: 10px'>Delete<i class='far fa-trash-alt'></i></div></a><a href='propertydetail/{row['id']}'><div class='badge badge-info'><i class='mdi mdi-eye'></i>Offerings</div></a>"


    class Meta:
        model = PropertyModel
        filter_form = PropertyFilterForm
        extra_fields = ('id', 'created_at', 'updated_at', 'status', 'price')


class PublicUserPropertyListDatatable(Datatable):
    name = column.StringColumn()
    saving = column.StringColumn()
    street = column.StringColumn()
    city = column.StringColumn()
    country = column.StringColumn()
    status = column.StringColumn()
    created_at = column.StringColumn()
    updated_at = column.StringColumn()
    view = column.StringColumn()

    def __init__(self, datapy=None):
        self.datapy = datapy

    def get_initial_queryset(self, request):
        self.datapy = json.loads(request.GET.get('extras'))
        return PublicUserPropertyModel.objects.filter(user_id=self.datapy['id'])

    def render_name(self, row):
        return PropertyModel.objects.get(id=row['properties_id']).name

    def render_saving(self, row):
        if Savings.objects.filter(user_id=row['user_id']).filter(properties_id=row['properties_id']).exists():
            return f'KSH '+ str(Savings.objects.filter(user_id=row['user_id']).get(properties_id=row['properties_id']).amount)
        return 'None'

    def render_street(self, row):
        return PropertyModel.objects.get(id=row['properties_id']).street

    def render_city(self, row):
        return PropertyModel.objects.get(id=row['properties_id']).city

    def render_country(self, row):
        return PropertyModel.objects.get(id=row['properties_id']).country

    def render_status(self, row):
        if PropertyModel.objects.get(id=row['properties_id']).status == 1:
            return f'Active'
        else:
            return f'Inactive'

    def render_created_at(self, row):
        return PropertyModel.objects.get(id=row['properties_id']).created_at.strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_updated_at(self, row):
        return PropertyModel.objects.get(id=row['properties_id']).updated_at.strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_view(self, row):
        return f"<a href='propertydetail/{row['properties_id']}'><div class='badge badge-info'><i class='mdi mdi-eye'></i>Offerings</div></a>"

    class Meta:
        model = PublicUserPropertyModel
        filter_form = PublicUserPropertyFilterForm
        extra_fields = ('id', 'properties_id', 'user_id')



class AvailablePublicUserPropertyListDatatable(Datatable):
    name = column.TextColumn()
    price = column.StringColumn()
    street = column.TextColumn()
    city = column.TextColumn()
    country = column.TextColumn()
    status = column.StringColumn()
    images = column.StringColumn()
    created_at = column.StringColumn()
    updated_at = column.StringColumn()
    action = column.StringColumn()

    def __init__(self, datapy=None):
        self.datapy = datapy

    def get_initial_queryset(self, request):
        self.datapy = json.loads(request.GET.get('extras'))
        query = PublicUserPropertyModel.objects.filter(user_id=self.datapy['id']).values_list('properties_id', flat=True)
        return PropertyModel.objects.exclude(id__in=query)

    def render_price(self, row):
        return f'KSH '+ str(row['price'])

    def render_status(self, row):
        if row['status'] == 1:
            return f'Active'
        else:
            return f'Inactive'

    def render_images(self, row):
        if PropertyPicturesModel.objects.filter(properties_id=row['id']).exists():
            return f"<div class='badge badge-success' data-toggle='modal' onClick='getImages({row['id']})' data-backdrop='static' data-keyboard='false' data-target='#propertyImages{row['id']}' style='margin-right: 10px'>View <i class='fas fa-edit'></i></div>"
        else:
            return f"<div class='badge badge-danger'>Upload Images</div>"

    def render_created_at(self, row):
        return row['created_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_updated_at(self, row):
        return row['updated_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_action(self, row):
        return f"<div class='badge badge-success' data-toggle='modal' data-target='#makePayment{row['id']}' style='margin-right: 10px'>Make Payment <i class='fas fa-edit'></i></div><a href='propertydetail/{row['id']}'><div class='badge badge-info'><i class='mdi mdi-eye'></i>Offerings</div></a>"

    class Meta:
        model = PropertyModel
        filter_form = PropertyFilterForm
        extra_fields = ('id', 'created_at', 'updated_at', 'status', 'street', 'name', 'price')


class PropertyDetailsListDatatable(Datatable):
    name = column.TextColumn()
    units = column.TextColumn()
    price = column.StringColumn()
    size = column.TextColumn()
    progress = column.StringColumn()
    status = column.TextColumn()
    created_at = column.StringColumn()
    updated_at = column.StringColumn()
    view = column.StringColumn()

    def __init__(self, datapy=None):
        self.datapy = datapy

    def get_initial_queryset(self, request):
        self.datapy = json.loads(request.GET.get('extras'))
        return PropertyDetailsModel.objects.filter(properties_id=self.datapy['id'])

    def render_price(self, row):
        return f'KSH '+ str(row['price'])

    def render_progress(self, row):
        return str(row['progress']) + f'% Done'

    def render_created_at(self, row):
        return row['created_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_updated_at(self, row):
        return row['updated_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_view(self, row):
        return f"<div class='badge badge-success' data-toggle='modal' data-target='#updateDetail{row['id']}' style='margin-right: 10px'>Edit <i class='fas fa-edit'></i></div><a href='{row['id']}/delete/{row['properties_id']}'><div class='badge badge-danger' style='margin-right: 10px'>Delete<i class='far fa-trash-alt'></i></div></a>"


    class Meta:
        model = PropertyDetailsModel
        filter_form = PropertyDetailsFilterForm
        extra_fields = ('id', 'created_at', 'updated_at', 'progress', 'price', 'properties_id')


class UserPropertyDetailsListDatatable(Datatable):
    name = column.TextColumn()
    units = column.TextColumn()
    price = column.StringColumn()
    size = column.TextColumn()
    progress = column.StringColumn()
    status = column.TextColumn()
    created_at = column.StringColumn()
    updated_at = column.StringColumn()

    def __init__(self, datapy=None):
        self.datapy = datapy

    def get_initial_queryset(self, request):
        self.datapy = json.loads(request.GET.get('extras'))
        return PropertyDetailsModel.objects.filter(properties_id=self.datapy['id'])

    def render_price(self, row):
        return f'KSH '+ str(row['price'])

    def render_progress(self, row):
        return str(row['progress']) + f'% Done'

    def render_created_at(self, row):
        return row['created_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    def render_updated_at(self, row):
        return row['updated_at'].strftime("%Y-%m-%d %I:%M:%S:%p")

    class Meta:
        model = PropertyDetailsModel
        filter_form = PropertyDetailsFilterForm
        extra_fields = ('id', 'created_at', 'updated_at', 'progress', 'price', 'properties_id')


class Property():
    @login_required
    def index(request):
        if request.user.user_role == 'Public':
            datatable = PublicUserPropertyListDatatable()
            datatable.extras = json.dumps({"id": request.user.id})
            return render(request, 'properties/publicuserproperty.html', { 
                'datatable': datatable,
            })
        elif request.user.user_role == 'Property Owner':
            datatable = UserPropertyListDatatable()
            datatable.extras = json.dumps({"id": request.user.id})
            properties = PropertyModel.objects.filter(user_id=request.user.id)
            return render(request, 'properties/userproperty.html', { 
                'datatable': datatable,
                'properties': properties,
            })

    @login_required
    def user_available_properties(request):
        datatable = AvailablePublicUserPropertyListDatatable()
        datatable.extras = json.dumps({"id": request.user.id})
        query = PublicUserPropertyModel.objects.filter(user_id=request.user.id).values_list('properties_id', flat=True)
        que = UserFinanciers.objects.filter(user_id=request.user.id).values_list('financier_id', flat=True)
        return render(request, 'properties/publicuserproperty.html', { 
            'datatable': datatable,
            'properties': PropertyModel.objects.exclude(id__in=query),
            'financiers': Financier.objects.filter(id__in=que)
        })


    @login_required
    def create(request):
        if request.method == 'POST':
            files = request.FILES.getlist('upload_file')
            create = PropertyModel(name=request.POST['name'], street=request.POST['street'], price=request.POST['price'], city=request.POST['city'], country=request.POST['country'], user_id=request.user.id, status=1, created_at=datetime.datetime.now(tz=pytz.UTC), updated_at=datetime.datetime.now(tz=pytz.UTC))
            create.save()
            for f in files:
                PropertyPicturesModel(name=f, properties=create, status=1, created_at=datetime.datetime.now(tz=pytz.UTC), updated_at=datetime.datetime.now(tz=pytz.UTC)).save() 
            messages.add_message(request, messages.SUCCESS,
                                'Property Created')
            return redirect('/property/user')

        return redirect('/property/user')


    @login_required
    def update(request, _id):
        if request.method == 'POST':
            update = PropertyModel.objects.get(id=_id)
            if request.POST['name'] is not None:
                update.name = request.POST['name']
            if request.POST['street'] is not None:
                update.street = request.POST['street']
            if request.POST['price'] is not None:
                update.price = request.POST['price']
            if request.POST['city'] is not None:
                update.city = request.POST['city']
            if request.POST['country'] is not None:
                update.country = request.POST['country']
            update.updated_at=datetime.datetime.now(tz=pytz.UTC)
            update.save()
            messages.add_message(request, messages.SUCCESS,
                                'Property Updated')
            return redirect('/property/user')

        return redirect('/property/user')


    @login_required
    def delete(request, _id):
        delete = PropertyModel.objects.get(id=_id)
        delete.delete()
        messages.add_message(request, messages.SUCCESS,
                                'Property Deleted')
        return redirect('/property/user')



    def get_property_images(request, _id):
        if PropertyPicturesModel.objects.filter(properties_id=_id).exists():
            pics = PropertyPicturesModel.objects.filter(properties_id=_id).values()
            return JsonResponse({'message': 'success', 'data': list(pics)}, status=200)
        return JsonResponse({'message': 'success', 'data': []}, status=200)



    def upload_property_images(request):
        if request.method == 'POST':
            files = request.FILES.getlist('images')
            prop = PropertyModel.objects.get(id=request.POST['propertyId'])
            for f in files:
                PropertyPicturesModel(name=f, properties=prop, status=1, created_at=datetime.datetime.now(tz=pytz.UTC), updated_at=datetime.datetime.now(tz=pytz.UTC)).save() 
            messages.add_message(request, messages.SUCCESS,
                                'Images Uploaded')
            return redirect('/property/user')

        return redirect('/property/user')




class PropertyDetails():
    @login_required
    def index(request, _id):
        if request.user.user_role == 'Property Owner':
            datatable = PropertyDetailsListDatatable()
            datatable.extras = json.dumps({"id": _id})
            details = PropertyDetailsModel.objects.filter(properties_id=_id)
            prop = PropertyModel.objects.filter(id=_id)
            return render(request, 'properties/propertydetails.html', { 
                'datatable': datatable,
                'details': details,
                'propertyId': _id,
                'properties': prop
            })
        else:
            datatable = UserPropertyDetailsListDatatable()
            datatable.extras = json.dumps({"id": _id})
            details = PropertyDetailsModel.objects.filter(properties_id=_id)
            prop = PropertyModel.objects.filter(id=_id)
            return render(request, 'properties/propertydetails.html', { 
                'datatable': datatable,
                'details': details,
                'propertyId': _id,
                'properties': prop
            })

    
    @login_required
    def create(request):
        if request.method == 'POST':
            create = PropertyDetailsModel(
                name=request.POST['name'], 
                size=request.POST['size'], 
                price=request.POST['price'], 
                properties_id=request.POST['propertyId'], 
                units=request.POST['units'], 
                status=request.POST['status'], 
                progress=request.POST['progress'], 
                created_at=datetime.datetime.now(tz=pytz.UTC), 
                updated_at=datetime.datetime.now(tz=pytz.UTC))
            create.save()
            messages.add_message(request, messages.SUCCESS,
                                'Property Offering Created')
            return redirect('propertyDetails', _id=request.POST['propertyId'])

        return redirect('propertyDetails', _id=request.POST['propertyId'])


    @login_required
    def update(request):
        if request.method == 'POST':
            update = PropertyDetailsModel.objects.get(id=request.POST['detailId'])
            if request.POST['name'] is not None:
                update.name = request.POST['name']
            if request.POST['size'] is not None:
                update.size = request.POST['size']
            if request.POST['price'] is not None:
                update.orice = request.POST['price']
            if request.POST['progress'] is not None:
                update.progress = request.POST['progress']
            if request.POST['propertyId'] is not None:
                update.properties_id = request.POST['propertyId']
            if request.POST['status'] is not None:
                update.status = request.POST['status']
            if request.POST['units'] is not None:
                update.units = request.POST['units']
            update.updated_at=datetime.datetime.now(tz=pytz.UTC)
            update.save()
            messages.add_message(request, messages.SUCCESS,
                                'Property Offerings Updated')
            return redirect('propertyDetails', _id=update.properties_id)

        return redirect('propertyDetails', _id=request.POST['propertyId'])


    @login_required
    def delete(request, _id, property_id):
        delete = PropertyDetailsModel.objects.get(id=_id)
        delete.delete()
        messages.add_message(request, messages.SUCCESS,
                                'Property Offering Deleted')
        return redirect('propertyDetails', _id=property_id)
    